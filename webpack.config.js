const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'radbse-auth-identity.js',
        library: '@radbse/auth-identity',
        libraryTarget: 'umd',
    },
    devtool: 'source-map',
    resolve: {
        alias: {
            react: path.resolve(__dirname, './node_modules/react'),
            'react-dom': path.resolve(__dirname, './node_modules/react-dom'),
        },
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'radbse-auth-identity.css',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.*css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader'],
                }),
            },
            {
                test: /\.(js|jsx)$/,
                use: ['babel-loader'],
                exclude: /node_modules/,
            },
        ],
    },
    externals: [
        '@radbse/hooks',
        '@radbse/auth',
        '@reach/router',
        'moment',
        'reselect',
        {
            react: {
                commonjs: 'react',
                commonjs2: 'react',
                amd: 'React',
                root: 'React',
            },
            'react-dom': {
                commonjs: 'react-dom',
                commonjs2: 'react-dom',
                amd: 'ReactDOM',
                root: 'ReactDOM',
            },
        },
    ],
}
