# @radbse/auth-identity

A JWT based asp.net identity client library used with the [Leaf.Rad.Bse](https://www.nuget.org/packages/Leaf.Rad.Bse/) dotnet template.

## install

```
npm install --save @radbse/auth-identity @radbse/auth @radbse/hooks
```

## Setup AuthProvider and SessionProvider

```
import React from 'react'
import { AuthProvider, getInitialAuthState, SessionProvider } from '@radbse/auth-identity'

const App = () => {
    const initialAuthState = getInitialAuthState()

    return (
        <AuthProvider initialState={initialAuthState}>
            <SessionProvider>
                ...
            <SessionProvider />
        </AuthProvider>
    )
}

export default App
```
